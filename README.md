# OpenML dataset: zoo

https://www.openml.org/d/62

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Richard S. Forsyth   
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Zoo) - 5/15/1990   
**Please cite**:  

**Zoo database**  
A simple database containing 17 Boolean-valued attributes describing animals.  The "type" attribute appears to be the class attribute. 

Notes:  
* I find it unusual that there are 2 instances of "frog" and one of "girl"!
* feature 'animal' is an identifier (though not unique) and should be ignored when modeling

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/62) of an [OpenML dataset](https://www.openml.org/d/62). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/62/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/62/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/62/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

